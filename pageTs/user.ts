import { userInfoByWx } from "../type/user";

class userInfoWxData implements userInfoByWx {
	nickname:string= "未登录";
	sex:number= 0;
	headimgurl:string= "../../static/user/user.png";
}

export {userInfoWxData}