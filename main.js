import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
//引入vuex的store
import vuexStore from './vuexStore'
export function createApp() {
  const app = createSSRApp(App)
  app.use(vuexStore)
  return {
    app
  }
}
// #endif