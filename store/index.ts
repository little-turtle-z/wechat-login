//状态集中管理
//数据实现响应式
//使用场景: 权限管理 路径 对用户进行权限限制
 
import { reactive } from "vue"
 
//ref-->字符串,数字 reactive-->对象中存储
const store={
  //定义状态
  state:reactive({
    nickname: "未登录",
    headimgurl: "../../static/user/user.png"
  }),
  toLoginByWx(){
   console.log("进入微信登录");
   	 // 获取微信服务商权限
	 // 在App平台，可用的服务商，是打包环境中配置的服务商，与手机端是否安装了该服务商的App没有关系。
   	 uni.getProvider({
   	 	service: 'oauth',
   	 	success: function (res :any) {
   	 		console.log(res.provider)
   	 		if (~res.provider.indexOf('weixin')) {
   				//如果支持微信登录 进行登录
   	 			uni.login({
   	 				"provider": "weixin",
   	 				"onlyAuthorize": true, // 微信登录仅请求授权认证
   	 				success: function(event :any){
   	 					//客户端成功获取授权临时票据（code）,向业务服务器发起登录请求。
   	 					uni.request({
   	 					    url: 'http://服务器IP:2210/wxLogin/user/getUserLogin', //仅为示例，并非真实接口地址。
   	 					    data: {
   	 					        code: event.code
   	 					    },
   	 					    success: (res :any) => {
   	 					        //获得用户信息保存并提示完成登录
   								store.state.nickname=res.data.nickname;
								store.state.headimgurl=res.data.headimgurl;
   								uni.setStorageSync("userInfo",res.data);
   								uni.showToast({
   								    title: "微信登录成功" ,
   								    icon: 'none'
   								});
   	 					    }
   	 					});
   	 				},
   	 				fail: function (err) {
   	 			        // 登录授权失败  
   	 			        // err.code是错误码
   						console.log(err);
   						uni.showToast({
   						    title: err.errMsg ,
   						    icon: 'none'
   						});
   	 			    }
   	 			})
   	 		}
   	 	},
   		//请求失败
   		fail: (err) => {
   			console.log(err);
   		    uni.showToast({
   		        title: "手机不支持微信登录" ,
   		        icon: 'none'
   		    });
   		},
   	 });
  },
  logoutByWx(){
  	 //1.清除本地缓存
	 uni.removeStorageSync('userInfo');
	 //2.state状态更替为默认
     store.state.headimgurl="../../static/user/user.png";
	 store.state.nickname="未登录";
  }
};
//在App组件通过provide提供
export default store