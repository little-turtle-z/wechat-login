export default {
	//开启命名空间后 访问所以属性都需要带模块名
	namespaced:true,
	state(){
		return {
			userInfo : null
		}
	},
	// 更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。
	mutations: {
		initInfo(state :any,info :any){
			state.userInfo=info
		}
	},
	actions: {
		userLoginByWx(context :any){
			console.log("进入微信登录");
				 // 获取微信服务商权限
				 uni.getProvider({
				 	service: 'oauth',
				 	success: function (res :any) {
				 		console.log(res.provider)
				 		if (~res.provider.indexOf('weixin')) {
							//如果支持微信登录 进行登录
				 			uni.login({
				 				"provider": "weixin",
				 				"onlyAuthorize": true, // 微信登录仅请求授权认证
				 				success: function(event :any){
				 					//客户端成功获取授权临时票据（code）,向业务服务器发起登录请求。
				 					uni.request({
				 					    url: 'http://服务器IP:2210/wxLogin/user/getUserLogin', //仅为示例，并非真实接口地址。
				 					    data: {
				 					        code: event.code
				 					    },
				 					    success: (res) => {
				 					        //获得用户信息完成登录
											context.commit('initInfo',res.data); 
				 							uni.setStorageSync('userInfoByVuex',res.data)
											
											uni.showToast({
											    title: "微信登录成功" ,
											    icon: 'none'
											});
				 					    }
				 					});
				 				},
				 				fail: function (err) {
				 			        // 登录授权失败  
				 			        // err.code是错误码
									console.log(err);
									uni.showToast({
									    title: err.errMsg ,
									    icon: 'none'
									});
				 			    }
				 			})
				 		}
				 	},
					//请求失败
					fail: (err) => {
						console.log(err);
					    uni.showToast({
					        title: "手机不支持微信登录" ,
					        icon: 'none'
					    });
					},
				 });
		
		},
		logout(context :any){
			//1.清除本地缓存
			uni.removeStorageSync('userInfoByVuex');
			//2.清除store仓库数据
			context.commit('initInfo',null);
		}
	}
}