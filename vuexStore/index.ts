import { createStore } from 'vuex'
import user from '@/vuexStore/user'
//创建store实例
const store = createStore({
	//引入模块
	modules:{
		user
	}
})
export default store


